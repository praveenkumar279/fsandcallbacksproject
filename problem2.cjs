const fs = require('fs');


function problem2(fileName, callBack){
    
    fs.readFile(fileName, (err, data) => {
        callBack(err,"File Read Successfully");

        const lipsumContent = data.toString();
        let lipsumContentInUpperCase = 'contentInUpperCase.txt';

        fs.writeFile(lipsumContentInUpperCase,lipsumContent.toUpperCase(), (err) => {    
            callBack(err, "Content converted to uppercase and created a new file with upperCase content");

                fs.appendFile('filenames.txt', lipsumContentInUpperCase + '\n', (err) => {
                    callBack(err, "UpperCase converted file name appended into filenames.txt");

                        fs.readFile(lipsumContentInUpperCase, (err, dataInUpperCase) => {
                            callBack(err, "Read the file and converted into lowercase and split into sentences");

                            let sentences = dataInUpperCase.toString().toLowerCase().split(".");
                            let lipsumContentInLowerCase = 'contentInLowerCase.txt';

                                fs.writeFile(lipsumContentInLowerCase, sentences.join("\n"), (err) => {
                                    callBack(err, "created a new file with lowercase content");

                                        fs.appendFile('filenames.txt', lipsumContentInLowerCase + '\n', (err) => {
                                            callBack(err,"Appended the lowercase content filename into filenames.txt");

                                                fs.readFile(lipsumContentInLowerCase, (err, data) => {
                                                    callBack(err,"Reading lowerCase content file");

                                                    let sortedData = data.toString()
                                                        .split("\n")
                                                        .sort((line1, line2) => {
                                                            return line1.localeCompare(line2);
                                                        })
                                                        .join("\n");
                                                        
                                                        let sortedLipsumDataFile = "contentInSortedOrder.txt";
    
                                                            fs.writeFile(sortedLipsumDataFile, sortedData, (err) => {
                                                                callBack(err,"created a new file with sorted content of lipsum text");

                                                                    fs.appendFile('filenames.txt', sortedLipsumDataFile + '\n', (err) => {
                                                                        callBack(err, "appended sorted filename into filenames.txt");

                                                                            fs.readFile('filenames.txt', (err, fileNamesData) => {
                                                                                callBack(err, "Reading filenames.txt file which contain all the other file names");
                                                                                    const fileNamesInArray = fileNamesData.toString()
                                                                                        .split("\n");
                                                                                    
                                                                                    fileNamesInArray.forEach((file) => {
                                                                                        if(file !== ''){
                                                                                            fs.unlink(file, (err) => {
                                                                                                callBack(err, "Deleted file in filenames.txt");
                                                                                            });
                                                                                        }      
                                                                                
                                                                                    });

                                                                            });

                                                                    });

                                                            }); 

                                                    });

                                            });  

                                    }); 

                            });

                    });

            });

    });
    
}


module.exports = problem2;
