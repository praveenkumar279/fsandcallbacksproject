const fs = require('fs');
const path = require('path');

function problem1(fileName, callBack) {
    fs.mkdir(fileName, (err) => {
        callBack(err, "Directory successfully created");

        const randomNumber = Math.floor(Math.random()*10) + 4;
        const sizeOfRandomNumbers = [...Array(randomNumber).keys()];

        sizeOfRandomNumbers.forEach((numbers) => {
            let fileName = 'file' + numbers + '.json';
            let jsonFileData = {
                id : numbers,
                name : fileName
            };
            console.log(fileName);

            fs.writeFile((path.join(__dirname, 'jsonFiles', fileName)), JSON.stringify(jsonFileData) , (err) => {
                callBack(err,"Successfully added json file");

                    fs.unlink(path.join('./jsonFiles', fileName), (err) => {
                        callBack(err, "Deleted file Successfully");
                    });       
            });
        });
    });
}


module.exports = problem1;


