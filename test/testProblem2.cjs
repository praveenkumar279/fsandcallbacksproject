const problem2 = require('../problem2.cjs');
const fileName = './lipsum.txt';


problem2(fileName, (err, data) => {
    if(err) {
        console.error(err);
    } else {
        console.log(data);
    }
});